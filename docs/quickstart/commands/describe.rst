Describe
========

The *Describe* command will show you how your environment is currently
configured. The output describing your environment can also be used to
configure and update your environment.

Examples
++++++++

Resource Group
--------------

List and Describe your Resource Groups:

.. code-block:: bash

    idem describe azure.resource_management.resource_groups

Output:

.. code-block:: bash

    /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1:
      azure.resource_management.resource_groups.present:
      - name: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
      - resource_id: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg1
      - resource_group_name: test-rg1
      - subscription_id: b8e40357-9bb6-4c41-9b43-a9ba0fd08160
      - location: eastus
      - tags:
          name: test-idem

The above example shows info about each Resource Group. In the next page discussing the
`Idem State </quickstart/commands/state>`_ we'll show how you can use the
output of the `Describe` command to configure the state of existing resources.

Role Definition
---------------

List and Describe your Role Definition

.. code-block:: bash

    idem describe azure.authorization.role_definitions

Output:

.. code-block:: bash

    /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/providers/Microsoft.Authorization/roleDefinitions/fb279b4f-3ba8-4d39-8e43-687eb2e7661c:
      azure.authorization.role_definitions.present:
      - name: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/providers/Microsoft.Authorization/roleDefinitions/fb279b4f-3ba8-4d39-8e43-687eb2e7661c
      - resource_id: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/providers/Microsoft.Authorization/roleDefinitions/fb279b4f-3ba8-4d39-8e43-687eb2e7661c
      - role_definition_id: fb279b4f-3ba8-4d39-8e43-687eb2e7661c
      - scope: /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160
      - role_definition_name: test-rd1
      - permissions:
        - actions:
          - Microsoft.Resources/subscriptions/resourceGroups/read
          dataActions: []
          notActions: []
          notDataActions: []
      - assignable_scopes:
      - /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160
      - description: Test Role Definition
