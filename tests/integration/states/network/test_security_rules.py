import copy
import uuid
from collections import ChainMap
from typing import List

import pytest


@pytest.mark.asyncio
async def test_security_rule(
    hub, ctx, resource_group_fixture, network_security_group_fixture
):
    """
    This test provisions a security rule, describes security rule and deletes
     the provisioned security rule.
    """
    # Create security rule with --test
    resource_group_name = resource_group_fixture.get("name")
    network_security_group_name = network_security_group_fixture.get("name")
    security_rule_name = "idem-test-security-rule-" + str(uuid.uuid4())
    parameters = {
        "protocol": "*",
        "source_address_prefix": "10.0.0.0/8",
        "destination_address_prefix": "11.0.0.0/8",
        "access": "Deny",
        "destination_port_range": "80",
        "source_port_range": "*",
        "priority": 105,
        "direction": "Outbound",
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.azure.network.security_rules.present(
        test_ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        **parameters,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would create azure.network.security_rules '{security_rule_name}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=security_rule_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}/securityRules/{security_rule_name}"
        == resource_id
    )

    # Create security rule in real
    ret = await hub.states.azure.network.security_rules.present(
        ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        **parameters,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=security_rule_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}/securityRules/{security_rule_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe security rule
    describe_ret = await hub.states.azure.network.security_rules.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get("azure.network.security_rules.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=resource_id,
    )

    # Update security rule in test
    updated_parameters = {
        "protocol": "*",
        "source_address_prefixes": ["12.0.0.0/8", "13.0.0.0/8"],
        "destination_address_prefix": "11.0.0.0/8",
        "access": "Allow",
        "destination_port_ranges": ["90", "100"],
        "source_port_range": "*",
        "priority": 115,
        "direction": "Outbound",
    }
    ret = await hub.states.azure.network.security_rules.present(
        test_ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        **updated_parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update azure.network.security_rules '{security_rule_name}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=parameters,
        expected_new_state=updated_parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=security_rule_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}/securityRules/{security_rule_name}"
        == resource_id
    )

    # Update security rule in real
    ret = await hub.states.azure.network.security_rules.present(
        ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        **updated_parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated azure.network.security_rules '{security_rule_name}'" in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=parameters,
        expected_new_state=updated_parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=security_rule_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}/securityRules/{security_rule_name}"
        == resource_id
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete security rule in test
    del_ret = await hub.states.azure.network.security_rules.absent(
        test_ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert del_ret.get("old_state") and not del_ret.get("new_state")
    assert (
        f"Would delete azure.network.security_rules '{security_rule_name}'"
        in del_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["old_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=security_rule_name,
    )

    # Delete security rule in real
    del_ret = await hub.states.azure.network.security_rules.absent(
        ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert del_ret.get("old_state") and not del_ret.get("new_state")
    assert (
        f"Deleted azure.network.security_rules '{security_rule_name}'"
        in del_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["old_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
        idem_resource_name=security_rule_name,
    )
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete again to check if the resource is absent
    del_ret = await hub.states.azure.network.security_rules.absent(
        ctx,
        name=security_rule_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        security_rule_name=security_rule_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert not del_ret.get("old_state") and not del_ret.get("new_state")
    assert (
        f"azure.network.security_rules '{security_rule_name}' already absent"
        in del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    network_security_group_name,
    security_rule_name,
    idem_resource_name,
):
    properties_params = [
        "protocol",
        "description",
        "source_port_range",
        "source_address_prefix",
        "destination_port_range",
        "destination_address_prefix",
        "access",
        "priority",
        "direction",
        "source_port_ranges",
        "destination_port_ranges",
        "source_address_prefixes",
        "destination_address_prefixes",
        "source_application_security_groups",
        "destination_application_security_groups",
    ]

    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert network_security_group_name == old_state.get(
            "network_security_group_name"
        )
        assert security_rule_name == old_state.get("security_rule_name")
        for param in properties_params:
            if isinstance(expected_old_state.get(param), List):
                assert sorted(expected_old_state.get(param)) == sorted(
                    old_state.get(param)
                )
            else:
                assert expected_old_state.get(param) == old_state.get(param)

    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert network_security_group_name == new_state.get(
            "network_security_group_name"
        )
        assert security_rule_name == new_state.get("security_rule_name")
        for param in properties_params:
            if isinstance(expected_new_state.get(param), List):
                assert sorted(expected_new_state.get(param)) == sorted(
                    new_state.get(param)
                )
            else:
                assert expected_new_state.get(param) == new_state.get(param)
