import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_public_ip_addresses(hub, ctx, resource_group_fixture):
    """
    This test provisions a public ip address, describes public ip addresses and deletes
     the provisioned public ip address.
    """
    # Create public ip address
    resource_group_name = resource_group_fixture.get("name")
    public_ip_address_name = "idem-test-pub-ip-" + str(int(time.time()))
    pub_ip_parameters = {
        "location": "eastus",
        "allocation_method": "Static",
        "idle_timeout_in_minutes": 10,
        "ip_version": "IPv4",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create public ip address with --test
    pub_ip_ret = await hub.states.azure.network.public_ip_addresses.present(
        test_ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        **pub_ip_parameters,
    )
    assert pub_ip_ret["result"], pub_ip_ret["comment"]
    assert not pub_ip_ret["old_state"] and pub_ip_ret["new_state"]
    assert (
        f"Would create azure.network.public_ip_addresses '{public_ip_address_name}'"
        in pub_ip_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=pub_ip_ret["new_state"],
        expected_old_state=None,
        expected_new_state=pub_ip_parameters,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        idem_resource_name=public_ip_address_name,
    )

    resource_id = pub_ip_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/publicIPAddresses/{public_ip_address_name}"
        == resource_id
    )

    # Create public ip address in real
    pub_ip_ret = await hub.states.azure.network.public_ip_addresses.present(
        ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        **pub_ip_parameters,
    )
    assert pub_ip_ret["result"], pub_ip_ret["comment"]
    assert not pub_ip_ret["old_state"] and pub_ip_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=pub_ip_ret["new_state"],
        expected_old_state=None,
        expected_new_state=pub_ip_parameters,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        idem_resource_name=public_ip_address_name,
    )
    resource_id = pub_ip_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/publicIPAddresses/{public_ip_address_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe virtual network
    describe_ret = await hub.states.azure.network.public_ip_addresses.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.network.public_ip_addresses.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        public_ip_address_name=public_ip_address_name,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_id,
    )

    pub_ip_update_parameters = {
        "location": "eastus",
        "allocation_method": "Static",
        "idle_timeout_in_minutes": 12,
        "ip_version": "IPv4",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
    }

    # Update public ip address with --test
    pub_ip_ret = await hub.states.azure.network.public_ip_addresses.present(
        test_ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        **pub_ip_update_parameters,
    )
    assert pub_ip_ret["result"], pub_ip_ret["comment"]
    assert pub_ip_ret["old_state"] and pub_ip_ret["new_state"]
    assert (
        f"Would update azure.network.public_ip_addresses '{public_ip_address_name}'"
        in pub_ip_ret["comment"]
    )
    check_returned_states(
        old_state=pub_ip_ret["old_state"],
        new_state=pub_ip_ret["new_state"],
        expected_old_state=pub_ip_parameters,
        expected_new_state=pub_ip_update_parameters,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        idem_resource_name=public_ip_address_name,
    )
    resource_id = pub_ip_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/publicIPAddresses/{public_ip_address_name}"
        == resource_id
    )

    # Update virtual network in real
    pub_ip_ret = await hub.states.azure.network.public_ip_addresses.present(
        ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        **pub_ip_update_parameters,
    )
    assert pub_ip_ret["result"], pub_ip_ret["comment"]
    assert pub_ip_ret["old_state"] and pub_ip_ret["new_state"]
    assert (
        f"Updated azure.network.public_ip_addresses '{public_ip_address_name}'"
        in pub_ip_ret["comment"]
    )
    check_returned_states(
        old_state=pub_ip_ret["old_state"],
        new_state=pub_ip_ret["new_state"],
        expected_old_state=pub_ip_parameters,
        expected_new_state=pub_ip_update_parameters,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        idem_resource_name=public_ip_address_name,
    )
    resource_id = pub_ip_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/publicIPAddresses/{public_ip_address_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete network interface in test
    pub_ip_del_ret = await hub.states.azure.network.public_ip_addresses.absent(
        test_ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
    )
    assert pub_ip_del_ret["result"], pub_ip_del_ret["comment"]
    assert pub_ip_del_ret["old_state"] and not pub_ip_del_ret["new_state"]
    assert (
        f"Would delete azure.network.public_ip_addresses '{public_ip_address_name}'"
        in pub_ip_del_ret["comment"]
    )
    check_returned_states(
        old_state=pub_ip_del_ret["old_state"],
        new_state=None,
        expected_old_state=pub_ip_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        idem_resource_name=public_ip_address_name,
    )

    # Delete network interface in real
    pub_ip_del_ret = await hub.states.azure.network.public_ip_addresses.absent(
        ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
    )
    assert pub_ip_del_ret["result"], pub_ip_del_ret["comment"]
    assert pub_ip_del_ret["old_state"] and not pub_ip_del_ret["new_state"]
    assert (
        f"Deleted azure.network.public_ip_addresses '{public_ip_address_name}'"
        in pub_ip_del_ret["comment"]
    )
    check_returned_states(
        old_state=pub_ip_del_ret["old_state"],
        new_state=None,
        expected_old_state=pub_ip_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
        idem_resource_name=public_ip_address_name,
    )
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete network interface again
    pub_ip_del_ret = await hub.states.azure.network.public_ip_addresses.absent(
        ctx,
        name=public_ip_address_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=public_ip_address_name,
    )
    assert pub_ip_del_ret["result"], pub_ip_del_ret["comment"]
    assert not pub_ip_del_ret["old_state"] and not pub_ip_del_ret["new_state"]
    assert (
        f"azure.network.public_ip_addresses '{public_ip_address_name}' already absent"
        in pub_ip_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    public_ip_address_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert public_ip_address_name == old_state.get("public_ip_address_name")
        if old_state.get("location") is not None:
            assert expected_old_state["location"] == old_state.get("location")
        if old_state.get("allocation_method") is not None:
            assert expected_old_state["allocation_method"] == old_state.get(
                "allocation_method"
            )
        if old_state.get("idle_timeout_in_minutes") is not None:
            assert expected_old_state["idle_timeout_in_minutes"] == old_state.get(
                "idle_timeout_in_minutes"
            )
        if old_state.get("ip_version") is not None:
            assert expected_old_state["ip_version"] == old_state.get("ip_version")
        if old_state.get("tags") is not None:
            assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert public_ip_address_name == new_state.get("public_ip_address_name")
        if new_state.get("location") is not None:
            assert expected_new_state["location"] == new_state.get("location")
        if new_state.get("allocation_method") is not None:
            assert expected_new_state["allocation_method"] == new_state.get(
                "allocation_method"
            )
        if new_state.get("idle_timeout_in_minutes") is not None:
            assert expected_new_state["idle_timeout_in_minutes"] == new_state.get(
                "idle_timeout_in_minutes"
            )
        if new_state.get("ip_version") is not None:
            assert expected_new_state["ip_version"] == new_state.get("ip_version")
        if new_state.get("tags") is not None:
            assert expected_new_state["tags"] == new_state.get("tags")
