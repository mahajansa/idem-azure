import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_key_vault_full(hub, ctx, resource_group_fixture):
    """
    This test provisions a key vault, describes key vault, does a force update and deletes
     the provisioned key vault.
    """
    # Create key vault
    resource_group_name = resource_group_fixture.get("name")
    vault_name = "vault-" + str(int(time.time()))
    fp_parameters = {
        "location": "eastus",
        "subscription_id": ctx.acct.subscription_id,
        "tags": {f"vault-" + str(uuid.uuid4()): f"vault-" + str(uuid.uuid4())},
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create vault with --test
    fp_ret = await hub.states.azure.key_vault.vault.present(
        test_ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        **fp_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert not fp_ret["old_state"] and fp_ret["new_state"]
    assert f"Would create azure.key_vault.vault '{vault_name}'" in fp_ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=fp_ret["new_state"],
        expected_old_state=None,
        expected_new_state=fp_parameters,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=vault_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.KeyVault/vaults/{vault_name}"
        == resource_id
    )

    # Create vault in real
    fp_ret = await hub.states.azure.key_vault.vault.present(
        ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        **fp_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert not fp_ret["old_state"] and fp_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=fp_ret["new_state"],
        expected_old_state=None,
        expected_new_state=fp_parameters,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=vault_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.KeyVault/vaults/{vault_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe vault
    describe_ret = await hub.states.azure.key_vault.vault.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get("azure.key_vault.vault.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=resource_id,
    )

    fp_update_parameters = {
        "location": "eastus",
        "subscription_id": ctx.acct.subscription_id,
        "tags": {
            f"idem-test-tag-key-update-"
            + str(uuid.uuid4()): f"idem-test-tag-update-value-"
            + str(uuid.uuid4())
        },
    }
    # Update vault tag with --test
    fp_ret = await hub.states.azure.key_vault.vault.present(
        test_ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        **fp_update_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert fp_ret["old_state"] and fp_ret["new_state"]
    assert f"Would update azure.key_vault.vault '{vault_name}'" in fp_ret["comment"]

    check_returned_states(
        old_state=fp_ret["old_state"],
        new_state=fp_ret["new_state"],
        expected_old_state=fp_parameters,
        expected_new_state=fp_update_parameters,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=vault_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.KeyVault/vaults/{vault_name}"
        == resource_id
    )

    # Update vault in real
    fp_ret = await hub.states.azure.key_vault.vault.present(
        ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        **fp_update_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert fp_ret["old_state"] and fp_ret["new_state"]
    assert f"Updated azure.key_vault.vault '{vault_name}'" in fp_ret["comment"]
    check_returned_states(
        old_state=fp_ret["old_state"],
        new_state=fp_ret["new_state"],
        expected_old_state=fp_parameters,
        expected_new_state=fp_update_parameters,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=vault_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.KeyVault/vaults/{vault_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete vault with --test
    vnet_del_ret = await hub.states.azure.key_vault.vault.absent(
        test_ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        subscription_id=ctx.acct.subscription_id,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"Would delete azure.key_vault.vault '{vault_name}'" in vnet_del_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=fp_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=vault_name,
    )

    # Delete vault in real
    vnet_del_ret = await hub.states.azure.key_vault.vault.absent(
        ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        subscription_id=ctx.acct.subscription_id,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert f"Deleted azure.key_vault.vault '{vault_name}'" in vnet_del_ret["comment"]
    check_returned_states(
        old_state=vnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=fp_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        idem_resource_name=vault_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete vault again
    vnet_del_ret = await hub.states.azure.key_vault.vault.absent(
        ctx,
        name=vault_name,
        resource_group_name=resource_group_name,
        vault_name=vault_name,
        subscription_id=ctx.acct.subscription_id,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert not vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"azure.key_vault.vault '{vault_name}' already absent"
        in vnet_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    vault_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert vault_name == old_state.get("vault_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert vault_name == new_state.get("vault_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")
