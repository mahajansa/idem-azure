from collections import ChainMap

import pytest

from tests.unit.idem_azure.states.azure import unit_test_utils
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST
from tests.unit.idem_azure.tool.azure.test_utils import PARAMETRIZE_TEST_AND_FLAG

RESOURCE_NAME = "my-resource"
RESOURCE_ID_TEMPLATE = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}"
RESOURCE_TYPE = "network.network_security_groups"
RESOURCE_ID_PROPERTIES = {
    "network_security_group_name": "my-network-security-group",
    "subscription_id": "my-subscription-id",
    "resource_group_name": "my-resource-group",
}
RESOURCE_GROUP_NAME = "my-resource-group"
NETWORK_SECURITY_GROUP_NAME = "my-security-group"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "security_rules": [
        {
            "name": "rule1",
            "access": "Allow",
            "destination_address_prefix": "*",
            "destination_port_range": "80",
            "direction": "Inbound",
            "priority": 130,
            "protocol": "*",
            "source_address_prefix": "*",
            "source_port_range": "*",
        }
    ],
}
RAW_RESOURCE_PARAMETERS = {
    "location": "eastus",
    "properties": {
        "securityRules": [
            {
                "name": "rule1",
                "properties": {
                    "access": "Allow",
                    "destinationAddressPrefix": "*",
                    "destinationPortRange": "80",
                    "direction": "Inbound",
                    "priority": 130,
                    "protocol": "*",
                    "sourceAddressPrefix": "*",
                    "sourcePortRange": "*",
                },
            }
        ]
    },
}
RESOURCE_PARAMETERS_UPDATE = {
    "location": "eastus",
    "security_rules": [
        {
            "name": "rule1",
            "access": "Deny",
            "destination_address_prefix": "*",
            "destination_port_range": "80",
            "direction": "Inbound",
            "priority": 150,
            "protocol": "*",
            "source_address_prefix": "*",
            "source_port_range": "*",
        }
    ],
}
RAW_RESOURCE_PARAMETERS_UPDATE = {
    "location": "eastus",
    "properties": {
        "securityRules": [
            {
                "name": "rule1",
                "properties": {
                    "access": "Deny",
                    "destinationAddressPrefix": "*",
                    "destinationPortRange": "80",
                    "direction": "Inbound",
                    "priority": 150,
                    "protocol": "*",
                    "sourceAddressPrefix": "*",
                    "sourcePortRange": "*",
                },
            }
        ]
    },
}


@pytest.fixture(scope="module", autouse=True)
def setup_subscription_id(ctx):
    RESOURCE_ID_PROPERTIES["subscription_id"] = ctx["acct"]["subscription_id"]


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, ctx):
    """
    Test 'present' state of network security groups. When a resource does not exist, 'present' should create the resource.
    """
    await unit_test_utils.test_present_resource_not_exists_create_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RAW_RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network security groups. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    await unit_test_utils.test_present_resource_exists_update_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RAW_RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS,
        RAW_RESOURCE_PARAMETERS_UPDATE,
        RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, ctx):
    """
    Test 'absent' state of network security groups. When a resource does not exist, 'absent' should just return success.
    """
    await unit_test_utils.test_absent_resource_not_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network security groups. When a resource exists, 'absent' should delete the resource.
    """
    await unit_test_utils.test_absent_resource_exists_only_request_mocks(
        hub,
        ctx,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_NAME,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RAW_RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of network security groups.
    """
    mock_hub.states.azure.network.network_security_groups.describe = (
        hub.states.azure.network.network_security_groups.describe
    )
    mock_hub.exec.azure.network.network_security_groups.list = (
        hub.exec.azure.network.network_security_groups.list
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present = (
        hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present
    )
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RAW_RESOURCE_PARAMETERS}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.network_security_groups.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.network.network_security_groups.present" in ret_value.keys()
    described_resource = ret_value.get("azure.network.network_security_groups.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state.get("tags") == old_state.get("tags")
        assert expected_old_state.get("security_rules") == old_state.get(
            "security_rules"
        )
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state.get("tags") == new_state.get("tags")
        assert expected_new_state.get("security_rules") == new_state.get(
            "security_rules"
        )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
# same behaviour with or without resource id flag
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_resource_id_not_passed_successful_create(
    hub, ctx, fake_acct_data, __test, __resource_id_flag
):
    unit_test_utils.test_resource_id_not_passed_successful_create(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RAW_RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST)
def test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
    hub, ctx, fake_acct_data, __test
):
    unit_test_utils.test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
        hub,
        ctx,
        fake_acct_data,
        __test,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
    )


# NOTE: this test mocks functions directly on the hub instead of mock hub.
# This should not mess with other tests since hub fixture is function scoped and reinitialized each time
@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RAW_RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_UPDATE,
        RAW_RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_update_without_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RAW_RESOURCE_PARAMETERS,
        RESOURCE_PARAMETERS_UPDATE,
        RAW_RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_with_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RAW_RESOURCE_PARAMETERS,
    )


@pytest.mark.parametrize(**PARAMETRIZE_TEST_AND_FLAG)
def test_empty_update_without_resource_id(
    hub, ctx, fake_acct_data, __test: bool, __resource_id_flag: bool
):
    unit_test_utils.test_empty_update_with_resource_id(
        hub,
        ctx,
        fake_acct_data,
        __test,
        __resource_id_flag,
        RESOURCE_ID_TEMPLATE,
        RESOURCE_TYPE,
        RESOURCE_ID_PROPERTIES,
        RESOURCE_PARAMETERS,
        RAW_RESOURCE_PARAMETERS,
    )
