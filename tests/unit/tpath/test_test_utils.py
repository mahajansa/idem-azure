import pytest

from tests.tpath.idem_azure_test.tool.azure.test_utils import get_items_at_path

test_dict = {
    "prop1": 0,
    "prop2": "somestr",
    "prop3": [],
    "prop4": [1, 2, 3],
    "prop5": {
        "nested_prop1": 1,
        "nested_prop2": ["element1", "element2"],
        "nested_prop3": {},
    },
    "prop6": None,
    "prop7": "",
}


def test_check_actual_includes_expected_equal_dicts(hub):
    hub.tool.azure.test_utils.check_actual_includes_expected(test_dict, test_dict, [])


def test_check_actual_includes_expected_additional_actual_props(hub):
    test_dict_extended = {
        **test_dict,
        "prop8": "new_prop",
        "prop5": {**test_dict.get("prop5"), "nested_prop4": "new_prop_nested"},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_extended, test_dict, []
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_nested_dict_item(hub):
    test_dict_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop1": 2},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_prop_changed, test_dict, []
    )


def test_check_actual_includes_expected_ignore_props_nested_dict_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop1": 2},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, ["prop5.nested_prop1"]
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_added_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {
            **test_dict.get("prop5"),
            "nested_prop2": ["element1", "element2", "element3"],
        },
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, []
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_changed_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop2": ["element1", "element100"]},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, []
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_removed_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop2": ["element1"]},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, []
    )


def test_check_actual_includes_expected_ignore_props_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {
            **test_dict.get("prop5"),
            "nested_prop2": ["element1", "element2", "element3"],
        },
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, ["prop5.nested_prop2"]
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_additional_expected_props_fails(hub):
    test_dict_extended = {
        **test_dict,
        "prop8": "new_prop",
        "prop5": {**test_dict.get("prop5"), "nested_prop4": "new_prop_nested"},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict, test_dict_extended, []
    )


def test_get_items_at_path():
    state = {"a": "b"}
    path = "a"
    expected_items = "b"
    assert get_items_at_path(state, path) == expected_items

    state = {"a": {"b": "c"}}
    path = "a"
    expected_items = {"b": "c"}
    assert get_items_at_path(state, path) == expected_items

    state = {"a": {"b": "c"}}
    path = "a.b"
    expected_items = "c"
    assert get_items_at_path(state, path) == expected_items

    state = {"a": [{"b": "c"}, {"b": "d"}]}
    path = "a[].b"
    expected_items = ["c", "d"]
    assert get_items_at_path(state, path) == expected_items

    state = {"a": [{"b": [{"c": "d"}, {"c": "e"}]}, {"b": [{"c": "f"}]}]}
    path = "a[].b[].c"
    expected_items = ["d", "e", "f"]
    assert get_items_at_path(state, path) == expected_items


def test_compare_nested_paths(hub):
    state_1 = {"a": [{"b": "c"}, {"b": "d"}]}
    state_2 = {"a": [{"b": "c"}, {"b": "d"}]}
    assert (
        hub.tool.azure.test_utils.compare_nested_paths(
            state_1, state_2, "a[].b", "a[].b"
        )
        is True
    )

    state_2["a"][0]["b"] = ["e"]
    assert (
        hub.tool.azure.test_utils.compare_nested_paths(
            state_1, state_2, "a[].b", "a[].b"
        )
        is False
    )

    state_2 = {"a": ["c", "d"]}
    assert (
        hub.tool.azure.test_utils.compare_nested_paths(state_1, state_2, "a[].b", "a")
        is True
    )


def test_get_nested_paths_comparator(hub):
    state_1 = {"a": [{"b": "c"}, {"b": "d"}], "x": "y"}
    state_2 = {"a": [{"b": "c"}, {"b": "d"}], "l": {"m": "y", "n": "z"}}
    comparator = hub.tool.azure.test_utils.get_nested_paths_comparator(state_1, state_2)
    assert comparator("a[].b", "a[].b") is True
    assert comparator("x", "l.m") is True
    assert comparator("x", "l.n") is False


def test_get_nested_paths_equality_asserter(hub):
    state_1 = {"a": [{"b": "c"}, {"b": "d"}], "x": "y"}
    state_2 = {"a": [{"b": "c"}, {"b": "d"}], "l": {"m": "y", "n": "z"}}
    asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        state_1, state_2
    )
    asserter("a[].b", "a[].b")
    asserter("x", "l.m")


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_get_nested_paths_equality_asserter_fail(hub):
    state_1 = {"a": [{"b": "c"}, {"b": "d"}], "x": "y"}
    state_2 = {"a": [{"b": "c"}, {"b": "d"}], "l": {"m": "y", "n": "z"}}
    asserter = hub.tool.azure.test_utils.get_nested_paths_equality_asserter(
        state_1, state_2
    )
    asserter("x", "l.n")
